;;;=Allows setf 
(require 'cl) 

;;;;=Disable-Menu-Bar 
(tool-bar-mode -1)  

;;;;=Personal Information 
(setf user-full-name "Jonathan Wot")
(setf user-mail-address "jonathantow2@gmail.com") 

;;;;=Themes and Default Startup Screen 
(setf inhibit-startup-screen t
      inhibit-startup-message t
      inhibit-startup-echo-area-message t) (setq auto-fill-mode 1) 
;;; Inhibit Error Bell 
;; make errors visible  
(setf visible-bell t)
(global-visual-line-mode 1); Proper line wrapping
(show-paren-mode 1); Matches parentheses and such in every mode
(setq calendar-week-start-day 1); Calender should start on Monday

;;=Default Screen Height
(add-to-list 'default-frame-alist '(height . 59)); Default frame height.

;;=Set colours
;; Set cursor and mouse-pointer colours 
(set-cursor-color "red") 
(set-mouse-color "goldenrod")
(add-to-list 'load-path "~/.emacs.d/plugins/smooth-scrolling")
(require' smooth-scrolling)

;;=Font
(set-face-attribute 'default nil :family "Optima Bold")
(set-face-attribute 'default nil :height 140)
;;=Theme-Mode
(add-to-list 'load-path "~/.emacs.d/plugins/colortheme")
 (require 'color-theme)
(setq color-theme-is-global t)
    (color-theme-initialize)
 (color-theme-standard)

;;;;=Packages
(setq package-archives '(("gnu" . "http://elpa.gnu.org/packages/")
                         ("marmalade" . "http://marmalade-repo.org/packages/")
                         ("melpa" . "http://melpa.milkbox.net/packages/")))

;;;;=History  
(setq savehist-file "~/.emacs.d/savehist")
(savehist-mode 1)
(setq history-length t)
(setq history-delete-duplicates t)
(setq savehist-save-minibuffer-history 1)
(setq savehist-additional-variables
      '(kill-ring
        search-ring
        regexp-search-ring))

;;;; Make Text mode the default mode for new buffers 
(setq default-major-mode 'text-mode)

;;=Spell Check
;;=Flyspell
(add-to-list 'load-path "~/.emacs.d/plugin/flyspell")
(setq ispell-program-name "/usr/local/bin/aspell")
(autoload 'flyspell-mode "flyspell" "On-the-fly spelling checker." t)
(add-hook 'message-mode-hook 'turn-on-flyspell)
(add-hook 'text-mode-hook 'turn-on-flyspell)
(add-hook 'Latex-mode-hook 'turn-on-flyspell)

;;;;=Auto Modes
;;=YasSnippet
(add-to-list 'load-path "~/.emacs.d/plugins/yasnippet/")
(require 'yasnippet )
(yas/initialize)
(setq yas/root-directory "~/.emacs.d/plugins/yasnippet/snippets/")
(yas/load-directory yas/root-directory)
(yas-global-mode 1)
;;; use popup menu for yas-choose-value
(add-to-list 'load-path "~/.emacs.d/plugins/popup")
(require 'popup)

;; add some shotcuts in popup menu mode
(define-key popup-menu-keymap (kbd "M-n") 'popup-next)
(define-key popup-menu-keymap (kbd "TAB") 'popup-next)
(define-key popup-menu-keymap (kbd "<tab>") 'popup-next)
(define-key popup-menu-keymap (kbd "<backtab>") 'popup-previous)
(define-key popup-menu-keymap (kbd "M-p") 'popup-previous)

(defun yas-popup-isearch-prompt (prompt choices &optional display-fn)
  (when (featurep 'popup)
    (popup-menu*
     (mapcar
      (lambda (choice)
        (popup-make-item
         (or (and display-fn (funcall display-fn choice))
             choice)
         :value choice))
      choices)
     :prompt prompt
     ;; start isearch mode immediately
     :isearch t
     )))
(setq yas-prompt-functions '(yas-popup-isearch-prompt yas-ido-prompt yas-no-prompt))

;;==Auto-Completion
(add-to-list 'load-path "~/.emacs.d/plugins/autocomplete")
(require 'auto-complete-config)
(add-to-list 'ac-dictionary-directories "~/.emacs.d/plugins/autocomplete/ac-dict")
(add-to-list 'ac-modes 'latex-mode)
(add-to-list 'ac-modes 'java-mode)
;;; activate, otherwise, auto-complete will
(ac-set-trigger-key "TAB")
(ac-set-trigger-key "<tab>")
(ac-config-default)

;;=Auto-Pair
(add-to-list 'load-path "~/.emacs.d/plugins/autopair")
(require 'autopair)
(autopair-global-mode 1)

;;=Auto-Indent
(electric-indent-mode 1)

;;=Auto-Neotree
(add-to-list 'load-path "~/.emacs.d/plugins/neotree")
(require 'neotree)
(global-set-key [f8] 'neotree-toggle)

;;=Auto-Linun
(add-hook 'java-mode-hook 
  (lambda ()
    (linum-mode 1)))
(add-hook 'c-mode-hook 
  (lambda ()
    (linum-mode 1)))
(add-hook 'c++-mode-hook 
  (lambda ()
    (linum-mode 1)))
(add-hook 'python-mode-hook 
  (lambda ()
    (linum-mode 1)))

;;;;=Ido-Mode 
(require 'ido)
(ido-mode t)
;;;;=Show-Parens-Mode
(show-paren-mode 1)
;;;;=Enable Speedbar
(add-to-list 'load-path "~/.emacs.d/plugins/srspeedbar")
(require 'sr-speedbar)

;;;;=Language-Modes
;;;;    ==== Scheme ====
 (setq scheme-program-name   "/usr/local/bin/mit-scheme")
 (add-to-list 'load-path "~/.emacs.d/plugins/scheme")
 (defun load-xscheme () (require 'xscheme)) 
 (add-hook 'scheme-mode-hook 'load-xscheme)
 (add-hook 'scheme-mode-hook 
  (lambda ()(linum-mode 1)))

;;;;    ==== Python ====
(package-initialize)
(elpy-enable)
(elpy-use-ipython)

;;;;    ==== Java =====
(add-to-list 'load-path "~/.emacs.d/plugins/autocomplete/ac-java/")
(require 'ajc-java-complete-config)
(add-hook 'java-mode-hook 'ajc-java-complete-mode)
(add-hook 'find-file-hook 'ajc-4-jsp-find-file-hook)

;;;;    ==== Haskell =====
(add-to-list 'load-path "~/.emacs.d/plugins/haskell-mode")
(require 'haskell-mode)

;;;;    ==== Javascript js2-mode ====
 (add-to-list 'load-path "~/.emacs.d/plugins/javascript")
 (require 'js2-mode)
 (add-hook 'js-mode-hook 'js2-minor-mode)
 (add-to-list 'auto-mode-alist '("\\.js\\'" . js-mode))
 (add-hook 'js2-mode-hook 
  (lambda ()
    (linum-mode 1)))

;;;;    ==== HTML & CSS ====

(add-hook 'html-mode-hook 
  (lambda ()
    (linum-mode 1)))

;;=Simple-Httpd 
 (add-to-list 'load-path "~/.emacs.d/plugins/simple-httpd")
 (require 'simple-httpd)

;;=Htmlize 
(add-to-list 'load-path "~/.emacs.d/plugins/htmlize")
(require 'htmlize)

;;=Impatient Mode 
(add-to-list 'load-path "~/.emacs.d/plugins/impatient-mode")
(require 'impatient-mode)


;;=Skewer Mode
 (add-to-list 'load-path "~/.emacs.d/plugins/skewer-mode-master")
 (require 'skewer-mode)

;;;; ==== Tex Mode Settings =================================================

;;=AucTex
(setenv "PATH" (concat (getenv "PATH") ":/usr/texbin"))

(add-to-list 'load-path "~/.emacs.d/plugins/autocomplete/ac-auctex")
(require 'auto-complete-auctex)

(custom-set-variables
 '(LaTeX-command "latex -synctex=1")
 '(TeX-view-program-list (quote (("Skim" "/Applications/Skim.app/Contents/SharedSupport/displayline %n %o %b") )))
)
(setq TeX-source-correlate-method 'synctex)
(add-hook 'LaTeX-mode-hook 'TeX-source-correlate-mode)

(setq TeX-parse-self t)
(setq-default TeX-master nil)
(add-hook 'LaTeX-mode-hook 'visual-line-mode)
(add-hook 'LaTeX-mode-hook 'flyspell-mode)
(add-hook 'LaTeX-mode-hook 'LaTeX-math-mode)
(add-hook 'LaTeX-mode-hook 'turn-on-reftex)
(setq reftex-plug-into-AUCTeX t)
(setq TeX-PDF-mode t)

;; Use Skim as viewer, enable source <-> PDF sync
;; make latexmk available via C-c C-c
;; Note: SyncTeX is setup via ~/.latexmkrc (see below)
(add-hook 'LaTeX-mode-hook (lambda ()
  (push
    '("latexmk" "latexmk -pdf %s" TeX-run-TeX nil t
      :help "Run latexmk on file")
    TeX-command-list)))
(add-hook 'TeX-mode-hook '(lambda () (setq TeX-command-default "latexmk")))

;; use Skim as default pdf viewer
;; Skim's displayline is used for forward search (from .tex to .pdf)
;; option -b highlights the current line; option -g opens Skim in the background  
(setq TeX-view-program-selection '((output-pdf "PDF Viewer")))
(setq TeX-view-program-list
     '(("PDF Viewer" "/Applications/Skim.app/Contents/SharedSupport/displayline -b -g %n %o %b")))
(setq Tex-engine 'pdflatex)
(setq TeX-PDF-mode t); PDF mode (rather than DVI-mode)

(add-hook 'TeX-mode-hook 'flyspell-mode); Enable Flyspell mode for TeX modes such as AUCTeX. Highlights all misspelled words.
(add-hook 'emacs-lisp-mode-hook 'flyspell-prog-mode); Enable Flyspell program mode for emacs lisp mode, which highlights all misspelled words in comments and strings.

(setq ispell-dictionary "english"); Default dictionary. To change do M-x ispell-change-dictionary RET.

(add-hook 'TeX-mode-hook
          (lambda () (TeX-fold-mode 1))); Automatically activate TeX-fold-mode.
(setq LaTeX-babel-hyphen nil); Disable language-specific hyphen insertion.

;; " expands into csquotes macros (for this to work babel must be loaded after csquotes).
(setq LaTeX-csquotes-close-quote "}"
      LaTeX-csquotes-open-quote "\\enquote{")

;; LaTeX-math-mode http://www.gnu.org/s/auctex/manual/auctex/Mathematics.html
(add-hook 'TeX-mode-hook 'LaTeX-math-mode)

;;;; ========================================================================
