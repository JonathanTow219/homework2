package controller;

import apptemplate.AppTemplate;
import data.GameData;
import gui.HangmanRenderer;
import gui.Workspace;
import javafx.animation.AnimationTimer;
import javafx.application.Platform;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.KeyEvent;

import javafx.scene.layout.*;

import javafx.scene.paint.*;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import propertymanager.PropertyManager;
import ui.AppMessageDialogSingleton;
import ui.YesNoCancelDialogSingleton;

import javafx.geometry.*;

import java.io.File;
import java.io.IOException;
import java.io.*;
import java.net.URL;
import java.nio.charset.CharacterCodingException;
import java.nio.file.Path;
import java.nio.file.Paths;

import hangman.Hangman;
import javafx.scene.Group;

import static settings.AppPropertyType.*;
import static settings.InitializationParameters.APP_WORKDIR_PATH;

/**
 * @author Ritwik Banerjee, Jonathan Tow
 */
public class HangmanController implements FileController {

    public enum GameState {
        UNINITIALIZED,
        INITIALIZED_UNMODIFIED,
        INITIALIZED_MODIFIED,
        ENDED
    }

    private AppTemplate appTemplate; // shared reference to the application
    private GameData    gamedata;    // shared reference to the game being played, loaded or saved
    private GameState   gamestate;   // the state of the game being shown in the workspace
    private Text[]      progress;    // reference to the text area for the word
    private boolean     success;     // whether or not player was successful
    private int         discovered;  // the number of letters already discovered
    private Button      gameButton;  // shared reference to the "start game" button
    private Button      hint;        // shared reference to the "hint" button
    private Label       remains;     // dynamically updated label that indicates the number of remaining guesses
    private Label       allGuessedLetters; // dynamically updated label that indicates all guessed letters
    private Path        workFile;

    private HangmanRenderer renderer;


    public HangmanController(AppTemplate appTemplate, Button gameButton) {
        this(appTemplate);
        this.gameButton = gameButton;
    }

    public HangmanController(AppTemplate appTemplate) {
        this.appTemplate = appTemplate;
        this.gamestate = GameState.UNINITIALIZED;

    }

    public void enableGameButton() {
        if (gameButton == null) {
            Workspace workspace = (Workspace) appTemplate.getWorkspaceComponent();
            gameButton = workspace.getStartGame();
        }
        gameButton.setDisable(false);
    }

    public void disableGameButton() {
        if (gameButton == null) {
            Workspace workspace = (Workspace) appTemplate.getWorkspaceComponent();
            gameButton = workspace.getStartGame();
        }
        gameButton.setDisable(true);
    }

    // Enables the hint button for user to get hint.
    public void enableHintButton() {
        if (hint == null) {
            Workspace workspace = (Workspace) appTemplate.getWorkspaceComponent();
            hint = workspace.getHint();
        }
        hint.setDisable(false);
        hint.setVisible(true);
    }
    public void disableHintButton() {
        if (hint == null) {
            Workspace workspace = (Workspace) appTemplate.getWorkspaceComponent();
            hint = workspace.getHint();
        }

        hint.setDisable(true);
        hint.setVisible(true);
    }

    public void setGameState(GameState gamestate) {
        this.gamestate = gamestate;
    }

    public GameState getGamestate() {
        return this.gamestate;
    }

    /**
     * In the homework code given to you, we had the line
     * gamedata = new GameData(appTemplate, true);
     * This meant that the 'gamedata' variable had access to the app, but the data component of the app was still
     * the empty game data! What we need is to change this so that our 'gamedata' refers to the data component of
     * the app, instead of being a new object of type GameData. There are several ways of doing this. One of which
     * is to write (and use) the GameData#init() method.
     */
    public void start() {
        gamedata = (GameData) appTemplate.getDataComponent();
        success = false;
        discovered = 0;

        Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();

        renderer =  gameWorkspace.getHangmanRenderer();
        renderer.clearCanvas();


        gamedata.init();
        setGameState(GameState.INITIALIZED_UNMODIFIED);
        HBox remainingGuessBox = gameWorkspace.getRemainingGuessBox();
        HBox guessedLetters    = (HBox) gameWorkspace.getGameTextsPane().getChildren().get(1);
        HBox totalGuessesBox = gameWorkspace.getTotalGuessesBox();


        remains = new Label(Integer.toString(GameData.TOTAL_NUMBER_OF_GUESSES_ALLOWED));
        remains.setId("remaincnt");

        Label rem = new Label("Remaining Guesses: ");
        rem.setId("remaincnt");


        remainingGuessBox.getChildren().addAll(rem, remains);
        remainingGuessBox.setMargin(rem, new Insets(25,0,0,0));
        remainingGuessBox.setMargin(remains, new Insets(25,0,0,0));


        allGuessedLetters = new Label(" ");
        Label g = new Label("Guessed Letters: ");
        allGuessedLetters.setId("guess");
        g.setId("guess");
        totalGuessesBox.getChildren().addAll(g, allGuessedLetters);


        if(gamedata.getTargetWord().length() > 7)
            enableHintButton();

        initWordGraphics(guessedLetters);
        play();
    }

    private void end() {
        appTemplate.getGUI().getPrimaryScene().setOnKeyTyped(null);
        gameButton.setDisable(true);

        if (hint != null)
            hint.setDisable(true);

        setGameState(GameState.ENDED);
        appTemplate.getGUI().updateWorkspaceToolbar(gamestate.equals(GameState.INITIALIZED_MODIFIED));

        Platform.runLater(() -> {
            PropertyManager           manager    = PropertyManager.getManager();
            AppMessageDialogSingleton dialog     = AppMessageDialogSingleton.getSingleton();
            String                    endMessage = manager.getPropertyValue(success ? GAME_WON_MESSAGE : GAME_LOST_MESSAGE);
            if (!success) {
                for (int i = 0; i < progress.length; i++) {
                    if (!progress[i].isVisible()) {
                        progress[i].setVisible(true);
                        progress[i].setId("missingtext");
                    }
                }
            }
            Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
            HBox endResult = gameWorkspace.getEndGameBox();
        });
    }


    private void initWordGraphics(HBox guessedLetters) {
        char[] targetword = gamedata.getTargetWord().toCharArray();

        progress = new Text[targetword.length];

        GridPane g = renderer.renderTargetWordSpace(progress,targetword);
        guessedLetters.getChildren().addAll(g);
        guessedLetters.setMargin(g, new Insets(20,0,20,0));
    }

    public void play() {
        disableGameButton();
        AnimationTimer timer = new AnimationTimer() {
            @Override
            public void handle(long now) {
                appTemplate.getGUI().updateWorkspaceToolbar(gamestate.equals(GameState.INITIALIZED_MODIFIED));
                appTemplate.getGUI().getPrimaryScene().setOnKeyTyped((KeyEvent event) -> {
                    char guess = event.getCharacter().charAt(0);
                    if (isAlpha(guess)) {

                        if (!alreadyGuessed(guess)) {
                            boolean goodguess = false;

                            for (int i = 0; i < progress.length; i++) {
                                if (gamedata.getTargetWord().charAt(i) == guess) {
                                    progress[i].setVisible(true);
                                    gamedata.addGoodGuess(guess);
                                    goodguess = true;
                                    discovered++;
                                }
                            }

                            if (!goodguess) {
                                gamedata.addBadGuess(guess);
                                // DRAW NEXT PART OF THE HANGMAN FIGURE
                                renderer.renderHangmanPart(gamedata.getRemainingGuesses());
                            }

                            success = (discovered == progress.length);
                            remains.setText(Integer.toString(gamedata.getRemainingGuesses()));
                            allGuessedLetters.setText(allGuessedLetters.getText() + " " + guess);
                        }
                    } else {
                       // AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                        //PropertyManager props = PropertyManager.getManager();
                        //dialog.show(props.getPropertyValue(ILLEGAL_KEY_TITLE),
                        //            props.getPropertyValue(ILLEGAL_KEY_MESSAGE));
                    }

                    setGameState(GameState.INITIALIZED_MODIFIED);
                });
                if (gamedata.getRemainingGuesses() <= 0 || success)
                    stop();
            }

            @Override
            public void stop() {
                super.stop();
                end();
            }
        };
        timer.start();
    }

    public boolean isAlpha(char c) {
        return (c >= 65 && c <= 90 || c >= 97 && c <= 122);
    }

    private void restoreGUI() {

        Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
        gameWorkspace.reinitialize();

        HBox guessedLetters = (HBox) gameWorkspace.getGameTextsPane().getChildren().get(1);
        restoreWordGraphics(guessedLetters);

        HBox remainingGuessBox = gameWorkspace.getRemainingGuessBox();

        HBox totalGuessesBox = gameWorkspace.getTotalGuessesBox();


        if (gamedata.isHinted()) {
            gamedata.decrementRemainingGuesses();
        }

        remains = new Label(Integer.toString(gamedata.getRemainingGuesses()));
        remains.setId("remaincnt");

        Label rem = new Label("Remaining Guesses: ");
        rem.setId("remaincnt");

        remainingGuessBox.getChildren().addAll(rem, remains);
        remainingGuessBox.setMargin(rem, new Insets(25,0,0,0));
        remainingGuessBox.setMargin(remains, new Insets(25,0,0,0));

        allGuessedLetters = new Label("");
        allGuessedLetters.setText(gamedata.getBadGuesses().toString().replaceAll("\\[", "")
                                            .replaceAll(",", "")
                                            .replaceAll("\\]", "")
                                  + " " +
                                  gamedata.getGoodGuesses().toString().replaceAll("\\[", "")
                                          .replaceAll(",", "")
                                          .replaceAll("\\]", ""));
        allGuessedLetters.setId("guess");
        Label g = new Label("Guessed Letters: ");
        g.setId("guess");

        totalGuessesBox.getChildren().addAll(g, allGuessedLetters);


        restoreHint(gamedata.isHinted());



        renderer =  gameWorkspace.getHangmanRenderer();
        renderer.clearCanvas();

        restoreHangmanParts(gamedata.getRemainingGuesses());

        success = false;
        play();
    }

    private void restoreHangmanParts(int remainingGuesses) {
        for (int i = 10; i >= remainingGuesses; i--)
            renderer.renderHangmanPart(i);
    }

    private void restoreHint(boolean hintUsed) {
        if (gamedata.getTargetWord().length()<=7)
            hint.setVisible(false);
        else if (hintUsed)
            disableHintButton();
        else
            enableHintButton();
    }

    // TODO REFACTOR the gridpane stuff.
    private void restoreWordGraphics(HBox guessedLetters) {
        discovered = 0;
        char[] targetword = gamedata.getTargetWord().toCharArray();
        progress = new Text[targetword.length];

        int main_row = 0;
        GridPane targetwordGrid = new GridPane();

        targetwordGrid.setGridLinesVisible(true);
        targetwordGrid.setId("grid");

        targetwordGrid.setPadding(new Insets(10, 0, 0, 0));

        // Constraints for the letter display columns
        ColumnConstraints colcon = new ColumnConstraints();
        colcon.setFillWidth(true);
        colcon.setMinWidth(23);


        for (int i = 0; i < progress.length; i++) {
            progress[i] = new Text(Character.toString(targetword[i]));
            progress[i].setVisible(gamedata.getGoodGuesses().contains(progress[i].getText().charAt(0)));
            if (progress[i].isVisible())
                discovered++;
            targetwordGrid.add(progress[i], i, main_row);
            targetwordGrid.setMargin(progress[i], new Insets(10, 0, 10, 8));
            targetwordGrid.getColumnConstraints().add(colcon);
        }

        guessedLetters.getChildren().addAll(targetwordGrid);
        guessedLetters.setMargin(targetwordGrid, new Insets(20,0,20,0));
    }

    private boolean alreadyGuessed(char c) {
        return gamedata.getGoodGuesses().contains(c) || gamedata.getBadGuesses().contains(c);
    }

    @Override
    public void handleNewRequest() {
        setGameState(GameState.UNINITIALIZED);
        AppMessageDialogSingleton messageDialog   = AppMessageDialogSingleton.getSingleton();
        PropertyManager           propertyManager = PropertyManager.getManager();
        boolean                   makenew         = true;
        if (gamestate.equals(GameState.INITIALIZED_MODIFIED))
            try {
                makenew = promptToSave();
            } catch (IOException e) {
                messageDialog.show(propertyManager.getPropertyValue(NEW_ERROR_TITLE), propertyManager.getPropertyValue(NEW_ERROR_MESSAGE));
            }
        if (makenew) {
            appTemplate.getDataComponent().reset();                // reset the data (should be reflected in GUI)
            appTemplate.getWorkspaceComponent().reloadWorkspace(); // load data into workspace
            ensureActivatedWorkspace();                            // ensure workspace is activated
            workFile = null;                                       // new workspace has never been saved to a file
            ((Workspace) appTemplate.getWorkspaceComponent()).reinitialize();
            enableGameButton();
            renderer =  ((Workspace) appTemplate.getWorkspaceComponent()).getHangmanRenderer();
            renderer.clearCanvas();
        }
        if (gamestate.equals(GameState.ENDED)) {
            appTemplate.getGUI().updateWorkspaceToolbar(false);
            Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
            gameWorkspace.reinitialize();
        }

    }

    @Override
    public void handleSaveRequest() throws IOException {
        PropertyManager propertyManager = PropertyManager.getManager();
        if (workFile == null) {
            FileChooser fileChooser = new FileChooser();
            URL workDirURL = Hangman.class.getClassLoader().getResource(APP_WORKDIR_PATH.getParameter());

            if (workDirURL == null)
                throw new FileNotFoundException("Work folder not found under resources.");

            File initialDir = new File(workDirURL.getFile());
            fileChooser.setInitialDirectory(initialDir);
            fileChooser.setTitle(propertyManager.getPropertyValue(SAVE_WORK_TITLE));
            String description = propertyManager.getPropertyValue(WORK_FILE_EXT_DESC);
            String extension = propertyManager.getPropertyValue(WORK_FILE_EXT);
            FileChooser.ExtensionFilter extFilter = new FileChooser.
                    ExtensionFilter(String.format("%s (*.%s)",
                    description, extension),
                    String.format("*.%s", extension));
            fileChooser.getExtensionFilters().add(extFilter);
            File selectedFile = fileChooser.showSaveDialog(appTemplate.getGUI().getWindow());
            if (selectedFile != null)
                save(selectedFile.toPath());
        } else
            save(workFile);
    }

    @Override
    public void handleLoadRequest() throws IOException {
        PropertyManager propertyManager = PropertyManager.getManager();
        try {
            boolean load = true;
            if (gamestate.equals(GameState.INITIALIZED_MODIFIED))
                load = promptToSave();
            if (load) {
                FileChooser fileChooser = new FileChooser();
                URL workDirURL = Hangman.class.getClassLoader().getResource(APP_WORKDIR_PATH.getParameter());

                if (workDirURL == null)
                    throw new FileNotFoundException("Work folder not found under resources.");

                File initialDir = new File(workDirURL.getFile());
                fileChooser.setInitialDirectory(initialDir);
                String description = propertyManager.getPropertyValue(WORK_FILE_EXT_DESC);
                String extension = propertyManager.getPropertyValue(WORK_FILE_EXT);
                FileChooser.ExtensionFilter extFilter = new FileChooser.
                        ExtensionFilter(String.format("%s (*.%s)",
                        description, extension),
                        String.format("*.%s", extension));
                fileChooser.getExtensionFilters().add(extFilter);
                File selectedFile = fileChooser.showOpenDialog(appTemplate.getGUI().getWindow());
                if (selectedFile != null && selectedFile.exists()) {
                    load(selectedFile.toPath());
                    restoreGUI(); // restores the GUI to reflect the state in which the loaded game was last saved
                }
            }
        } catch (IOException e) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            PropertyManager props = PropertyManager.getManager();
            dialog.show(props.getPropertyValue(LOAD_ERROR_TITLE),
                    props.getPropertyValue(LOAD_ERROR_MESSAGE));
        } catch (NullPointerException e) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            PropertyManager props = PropertyManager.getManager();
            dialog.show(props.getPropertyValue(LOAD_ERROR_TITLE),
                    props.getPropertyValue(LOAD_ERROR_MESSAGE));
        }
    }

    @Override
    public void handleExitRequest() {
        try {
            boolean exit = true;
            if (gamestate.equals(GameState.INITIALIZED_MODIFIED))
                exit = promptToSave();
            if (exit)
                System.exit(0);
        } catch (IOException ioe) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            PropertyManager           props  = PropertyManager.getManager();
            dialog.show(props.getPropertyValue(SAVE_ERROR_TITLE), props.getPropertyValue(SAVE_ERROR_MESSAGE));
        }
    }


    public void handleHint() {


        int targetLettersLeft = gamedata.getTargetWordLetters().size()
                                - gamedata.getGoodGuesses().size();

        if (targetLettersLeft > 1 && gamedata.getRemainingGuesses() > 1) {
            char hintLetter = 0;
            for (char c : gamedata.getTargetWordLetters()) {
                if (!gamedata.getGoodGuesses().contains(c)) {
                    hintLetter = c;
                    break;
                }
            }

            for (int i = 0; i < progress.length; i++) {
                if (progress[i].getText().toCharArray()[0] == hintLetter) {
                    progress[i].setVisible(true);
                    discovered++;
                }
            }

            gamedata.decrementRemainingGuesses();
            renderer.renderHangmanPart(gamedata.getRemainingGuesses());

            remains.setText(Integer.toString(gamedata.getRemainingGuesses()));
            gamedata.addGoodGuess(hintLetter);
            allGuessedLetters.setText(allGuessedLetters.getText() + " " + hintLetter);

            gamedata.setHinted(true);

            disableHintButton();

        } else {
            // say something like user is cheating.
        }

        setGameState(GameState.INITIALIZED_MODIFIED);

    }

    private void ensureActivatedWorkspace() {
        appTemplate.getWorkspaceComponent().activateWorkspace(appTemplate.getGUI().getAppPane());
    }

    private boolean promptToSave() throws IOException {
        PropertyManager            propertyManager   = PropertyManager.getManager();
        YesNoCancelDialogSingleton yesNoCancelDialog = YesNoCancelDialogSingleton.getSingleton();

        yesNoCancelDialog.show(propertyManager.getPropertyValue(SAVE_UNSAVED_WORK_TITLE),
                               propertyManager.getPropertyValue(SAVE_UNSAVED_WORK_MESSAGE));

        if (yesNoCancelDialog.getSelection().equals(YesNoCancelDialogSingleton.YES))
            handleSaveRequest();

        return !yesNoCancelDialog.getSelection().equals(YesNoCancelDialogSingleton.CANCEL);
    }

    /**
     * A helper method to save work. It saves the work, marks the current work file as saved, notifies the user, and
     * updates the appropriate controls in the user interface
     *
     * @param target The file to which the work will be saved.
     * @throws IOException
     */
    private void save(Path target) throws IOException {
        appTemplate.getFileComponent().saveData(appTemplate.getDataComponent(), target);
        workFile = target;
        setGameState(GameState.INITIALIZED_UNMODIFIED);
        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
        PropertyManager           props  = PropertyManager.getManager();
        dialog.show(props.getPropertyValue(SAVE_COMPLETED_TITLE), props.getPropertyValue(SAVE_COMPLETED_MESSAGE));
    }

    /**
     * A helper method to load saved game data. It loads the game data, notified the user, and then updates the GUI to
     * reflect the correct state of the game.
     *
     * @param source The source data file from which the game is loaded.
     * @throws IOException
     */
    private void load(Path source) throws IOException {
        // load game data
        appTemplate.getFileComponent().loadData(appTemplate.getDataComponent(), source);

        // set the work file as the file from which the game was loaded
        workFile = source;

        // notify the user that load was successful
        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
        PropertyManager           props  = PropertyManager.getManager();
        dialog.show(props.getPropertyValue(LOAD_COMPLETED_TITLE), props.getPropertyValue(LOAD_COMPLETED_MESSAGE));

        setGameState(GameState.INITIALIZED_UNMODIFIED);
        Workspace gameworkspace = (Workspace) appTemplate.getWorkspaceComponent();
        ensureActivatedWorkspace();
        gameworkspace.reinitialize();
        gamedata = (GameData) appTemplate.getDataComponent();
    }
}
