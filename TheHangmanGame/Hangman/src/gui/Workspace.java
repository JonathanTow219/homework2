package gui;

import apptemplate.AppTemplate;
import components.AppWorkspaceComponent;
import controller.HangmanController;
import hangman.Hangman;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ToolBar;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import propertymanager.PropertyManager;
import ui.AppGUI;

// ADDED BY JON
import javafx.scene.layout.*;
import javafx.scene.paint.*;

import java.io.IOException;

import static hangman.HangmanProperties.*;
import javafx.scene.canvas.*;
import javafx.scene.*;
import javafx.geometry.Insets;
/**
 * This class serves as the GUI component for the Hangman game.
 *
 * @author Ritwik Banerjee, Jonathan Tow
 */
public class Workspace extends AppWorkspaceComponent {

    AppTemplate app; // the actual application
    AppGUI      gui; // the GUI inside which the application sits

    Label             guiHeadingLabel;   // workspace (GUI) heading label
    HBox              headPane;          // conatainer to display the heading
    HBox              bodyPane;          // container for the main game displays
    ToolBar           footToolbar;       // toolbar for game buttons
    BorderPane        figurePane;        // container to display the namesake graphic of the (potentially) hanging person
    VBox              gameTextsPane;     // container to display the text-related parts of the game
    HBox              guessedLetters;    // text area displaying all the letters guessed correctly so far
    HBox              remainingGuessBox; // container to display the number of remaining guesses
    HBox              totalGuessesBox;   // container to display all guess ltters
    HBox              endGameBox;        // container to display end result
    Button            startGame;         // the button to start playing a game of Hangman
    Button            hint;              // the button to get a hint
    HangmanController controller;

    HangmanRenderer   renderer;

    /**
     * Constructor for initializing the workspace, note that this constructor
     * will fully setup the workspace user interface for use.
     *
     * @param initApp The application this workspace is part of.
     * @throws IOException Thrown should there be an error loading application
     *                     data for setting up the user interface.
     */
    public Workspace(AppTemplate initApp) throws IOException {
        app = initApp;
        gui = app.getGUI();
        controller = (HangmanController)
                        gui.getFileController();    //new HangmanController(app, startGame); <-- THIS WAS A MAJOR BUG!??
        layoutGUI();     // initialize all the workspace (GUI) components including the containers and their layout
        setupHandlers(); // ... and set up event handling
    }

    private void layoutGUI() {
        PropertyManager propertyManager = PropertyManager.getManager();
        guiHeadingLabel = new Label(propertyManager.getPropertyValue(WORKSPACE_HEADING_LABEL));

        headPane = new HBox();
        headPane.getChildren().add(guiHeadingLabel);
        headPane.setAlignment(Pos.CENTER);

        // TODO the frame  for the hanging person
        figurePane = new BorderPane();

        guessedLetters = new HBox();
        guessedLetters.setStyle("-fx-background-color: transparent;");

        remainingGuessBox = new HBox();

        totalGuessesBox = new HBox();

        gameTextsPane = new VBox();

        endGameBox = new HBox();

        hint = new Button("HINT");
        hint.setId("hintbtn");
        gameTextsPane.setMargin(hint, new Insets(10,0,10,0));
        // TODO gameTextsPane container to display the text-related parts of the game
        gameTextsPane.getChildren().setAll(remainingGuessBox, guessedLetters,
                                            totalGuessesBox, hint, endGameBox);

        renderer = new HangmanRenderer();
        // HANDLE FIGURE STUFF
        Group r = new Group();
        r.getChildren().add(renderer.getHangmanCanvas());
        // PUT THE CANVAS IN A CONTAINER
        figurePane.setCenter(r);

        bodyPane = new HBox();

        // TODO Body pane for Main Game
        bodyPane.getChildren().addAll(gameTextsPane, figurePane);

        startGame = new Button("Start Playing");
        HBox blankBoxLeft  = new HBox();
        HBox blankBoxRight = new HBox();
        HBox.setHgrow(blankBoxLeft, Priority.ALWAYS);
        HBox.setHgrow(blankBoxRight, Priority.ALWAYS);
        footToolbar = new ToolBar(blankBoxLeft, startGame, blankBoxRight);

        workspace = new VBox();
        workspace.getChildren().addAll(headPane, bodyPane, footToolbar);
    }

    private void setupHandlers() {
        startGame.setOnMouseClicked(e -> controller.start());
        hint.setOnMouseClicked(e -> controller.handleHint());
    }

    /**
     * This function specifies the CSS for all the UI components known at the time the workspace is initially
     * constructed. Components added and/or removed dynamically as the application runs need to be set up separately.
     */
    @Override
    public void initStyle() {
        PropertyManager propertyManager = PropertyManager.getManager();

        gui.getAppPane().setId(propertyManager.getPropertyValue(ROOT_BORDERPANE_ID));
        gui.getToolbarPane().getStyleClass().setAll(propertyManager.getPropertyValue(SEGMENTED_BUTTON_BAR));
        gui.getToolbarPane().setId(propertyManager.getPropertyValue(TOP_TOOLBAR_ID));

        ObservableList<Node> toolbarChildren = gui.getToolbarPane().getChildren();
        toolbarChildren.get(0).getStyleClass().add(propertyManager.getPropertyValue(FIRST_TOOLBAR_BUTTON));
        toolbarChildren.get(toolbarChildren.size() - 1).getStyleClass().add(propertyManager.getPropertyValue(LAST_TOOLBAR_BUTTON));

        workspace.getStyleClass().add(CLASS_BORDERED_PANE);
        guiHeadingLabel.getStyleClass().setAll(propertyManager.getPropertyValue(HEADING_LABEL));

    }

    /** This function reloads the entire workspace */
    @Override
    public void reloadWorkspace() {
        /* does nothing; use reinitialize() instead */
    }

    public VBox getGameTextsPane() {
        return gameTextsPane;
    }

    public HBox getRemainingGuessBox() {
        return remainingGuessBox;
    }

    public HBox getTotalGuessesBox() { return totalGuessesBox;}

    public HBox getEndGameBox() {return endGameBox;}

    public BorderPane getFigurePane() {return  figurePane;}

    public Button getHint() {return hint;}

    public Button getStartGame() {
        return startGame;
    }

    public HangmanRenderer getHangmanRenderer() {
        return renderer;
    }

    public void reinitialize() {
        guessedLetters = new HBox();
        guessedLetters.setStyle("-fx-background-color: transparent;");
        remainingGuessBox = new HBox();
        totalGuessesBox = new HBox();
        endGameBox = new HBox();
        gameTextsPane = new VBox();

        hint.setVisible(false);
        // CLEAR OLD FIGURE FROM GAME
        //figurePane = new BorderPane();
        gameTextsPane.getChildren().addAll(remainingGuessBox, guessedLetters,
                                            totalGuessesBox, hint, endGameBox);
        bodyPane.getChildren().setAll(figurePane, gameTextsPane);
    }


}
