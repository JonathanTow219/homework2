package gui;

import java.util.ArrayList;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Point2D;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.layout.HBox;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Paint;
import javafx.scene.text.Text;

import javafx.scene.layout.*;

/**
 * Created by drseuss on 10/3/16.
 * @author Jonathan Tow
 */
public class HangmanRenderer {
    private Canvas canvas;
    private GraphicsContext gc;
    private Point2D location;
    private String outlineColor = "black";

    public HangmanRenderer() {
        location = new Point2D(50,50);
        canvas = new Canvas();

        canvas.setStyle("-fx-background-color: black");
        canvas.setWidth(400);
        canvas.setHeight(375);
        gc =  canvas.getGraphicsContext2D();
    }


    public Canvas getHangmanCanvas() {
        return canvas;
    }

    public void clearCanvas() {
        gc.clearRect(0,0,canvas.getWidth(), canvas.getHeight());
    }

    public void renderHangmanPart (int remainingGuesses) {
        double x_loc = location.getX();
        double y_loc = location.getY();

        switch (remainingGuesses) {
            case 10:
                break;
            case 9: // FLOOR
                double floor_w = 250;
                double floor_h = 10;
                double floor_x = x_loc + 50;
                double floor_y = y_loc + 280;

                gc.setFill(Paint.valueOf(outlineColor));
                gc.fillRect(floor_x, floor_y, floor_w, floor_h);
                gc.beginPath();
                gc.stroke();
                break;
            case 8: // STAND
                double stand_w = 10;
                double stand_h = y_loc + 260;
                double stand_x = x_loc + 40;
                double stand_y = y_loc-20;

                gc.setFill(Paint.valueOf(outlineColor));
                gc.fillRect(stand_x, stand_y, stand_w, stand_h);
                gc.beginPath();
                gc.stroke();
                break;
            case 7: // TOP HOLD
                double top_w = 150;
                double top_h = 10;
                double top_x = x_loc + 50;
                double top_y = y_loc-20;

                gc.setFill(Paint.valueOf(outlineColor));
                gc.fillRect(top_x, top_y, top_w, top_h);
                gc.beginPath();
                gc.stroke();
                break;
            case 6: //ROPE
                double rope_w = 10;
                double rope_h = 30;
                double rope_x = x_loc + 165;
                double rope_y = y_loc - 20;

                gc.setFill(Paint.valueOf(outlineColor));
                gc.fillRect(rope_x,rope_y,rope_w,rope_h);
                gc.beginPath();
                gc.stroke();
                break;
            case 5: // HEAD
                int headW = 70;
                int headH = 70;
                double head_x = x_loc + 135;
                double head_y = y_loc + 10;

                // DRAW HIS RED HEAD
                //gc.setFill(Paint.valueOf(headColor));
                gc.fillOval(head_x, head_y, headW, headH);
                gc.beginPath();
                gc.setStroke(Paint.valueOf(outlineColor));
                gc.setLineWidth(1);
                gc.stroke();
                break;
            case 4: // BODY
                double body_w = 10;
                double body_h = 100;
                double body_x = x_loc + 165;
                double body_y = y_loc + 80;

                gc.setFill(Paint.valueOf(outlineColor));
                gc.fillRect(body_x,body_y,body_w,body_h);
                gc.beginPath();
                gc.fillOval(body_x-5,body_y, 20, 10);
                gc.stroke();
                break;
            case 3: // R_ARM
                double rarm_w = 10;
                double rarm_h = 40;
                double rarm_x = x_loc;
                double rarm_y = y_loc+215;

                gc.save();
                gc.rotate(315);
                gc.setFill(Paint.valueOf(outlineColor));
                gc.fillRect(rarm_x, rarm_y,rarm_w,rarm_h);
                gc.beginPath();
                gc.stroke();
                gc.restore();
                break;
            case 2: // L_ARM
                double larm_w = 10;
                double larm_h = 40;
                double larm_x = x_loc+200;
                double larm_y = y_loc-95;

                gc.save();
                gc.rotate(45);
                gc.setFill(Paint.valueOf(outlineColor));
                gc.fillRect(larm_x, larm_y,larm_w,larm_h);
                gc.beginPath();
                gc.stroke();
                gc.restore();
                break;
            case 1: // R_LEG
                double rleg_w = 10;
                double rleg_h = 60;
                double rleg_x = x_loc-54;
                double rleg_y = y_loc+264;

                gc.save();
                gc.rotate(315);
                gc.setFill(Paint.valueOf(outlineColor));
                gc.fillRect(rleg_x, rleg_y,rleg_w,rleg_h);
                gc.beginPath();
                gc.stroke();
                gc.restore();
                break;
            case 0: // L_LEG
                double lleg_w = 10;
                double lleg_h = 60;
                double lleg_x = x_loc+256;
                double lleg_y = y_loc-47;

                gc.save();
                gc.rotate(45);
                gc.setFill(Paint.valueOf(outlineColor));
                gc.fillRect(lleg_x, lleg_y,lleg_w,lleg_h);
                gc.beginPath();
                gc.stroke();
                gc.restore();
                break;

        }
    }

    /*
        Renders the target word space in a GridPane
     */
    public GridPane renderTargetWordSpace(Text[] progress, char[] targetword) {
        GridPane targetwordGrid = new GridPane();
        targetwordGrid.setGridLinesVisible(true);


        targetwordGrid.setPadding(new Insets(10, 0, 0, 0));

        // Constraints for the letter display columns
        ColumnConstraints colcon = new ColumnConstraints();
        colcon.setFillWidth(true);
        colcon.setMinWidth(23);

        int main_row = 0;
        for (int i = 0; i < progress.length; i++) {
            progress[i] = new Text(Character.toString(targetword[i]));
            progress[i].setVisible(false);
            targetwordGrid.add(progress[i], i, main_row);
            // sets text letter to "center" of its cell
            targetwordGrid.setMargin(progress[i], new Insets(10, 0, 10, 8));
            targetwordGrid.getColumnConstraints().add(colcon);

        }

        return targetwordGrid;
    }


}

