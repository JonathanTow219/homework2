/* Ex. 1-16
 * Solution: Revise the main routine of the longest-line program so it will correctly print the length of arbitrarily long input lines, and as much as possible of the text. 
 */
#include <stdio.h>
#define MAXLINE 10

int get_line(char s[], int lim);
void copy(char to[], char from[]);

/* print longest input line */
int main(void) {
  int len, max;
  char line[MAXLINE];
  char longest[MAXLINE];

  printf("Enter control-D to get results\n");
  
  max = 0;
  while ((len = get_line(line, MAXLINE)) > 0) {
    if (len > max) {
      max = len;
      copy(longest, line);
    }    
  }

  if (max > 0) {
    printf("\nLongest Line: %s\nLength of Line: %d\n", longest, max);
  }

  return 0;
}

/* getline: read a line into s, return length */
int get_line(char s[], int lim) {
  int c, i, leftover;
 
  i = leftover = 0;

  while ((c = getchar()) != EOF && c != '\n') {
    if (i < lim-1) {
      s[i] = c;
      ++i;
    } else {
      ++leftover;
    }
  }

  if (c == '\n') {
    s[i] = c;
    ++i;
  }
  s[i] = '\0';

  return i + leftover;
}

/* copy: copy 'from' into 'to'; assume 'to' is big enough */    
void copy(char to[], char from[]) {
  int i;

  i = 0;
  while ((to[i] = from[i]) != '\0') 
    ++i;
}
