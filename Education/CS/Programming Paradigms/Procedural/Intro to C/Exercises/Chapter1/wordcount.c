
#include <stdio.h>

#define IN 0 // inside a word
#define OUT 1 // outside a word

int main(void) {

  int c, nl, nw, nc, state;
  
  state = OUT;
  nl = nw = nc = 0;

  while ((c = getchar()) != EOF) {
    ++nc;
    if (c == '\n') 
      ++nl;
    if (c == ' ' || c == '\t' || c == '\n') 
      state = OUT;
    else if (state == OUT) { 
      state = IN;
      ++nw;
    }
  }
  
  printf("newlines: %d, words:  %d, characters: %d\n", nl, nw, nc);

}
