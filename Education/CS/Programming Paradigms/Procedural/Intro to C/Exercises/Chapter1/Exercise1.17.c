/*
 * Ex 1-17
 * Solution: Write a program to print all input lines that are longer than 80 characters. 
 */

#include <stdio.h>
#define PRINT_LIM 80
#define MAXLINE 10000

/* function prototypes */
int get_line(char s[], int lim);

/*print lines longer than 80 charcheters*/
int main(void) {
  
  int len;
  char line[MAXLINE];
   
  printf("Enter control-D to get results\n");

  while ((len = get_line(line, MAXLINE)) > 0) {
    if (len > PRINT_LIM) 
      printf("\n\nLine Longer than 80 chars: \n%s\n\n", line);   
  }
}

/* get_line: read a line into s, return length */
int get_line(char s[], int lim) {
  int c, i;
  
  for (i = 0; i < lim-1 && (c = getchar()) != EOF && c != '\n'; ++i) 
    s[i] = c;
  
  return i;
}
