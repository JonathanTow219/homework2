/* Ex. 1-10
 * Solution: replace each tab by \t, 
 * each backspace by \b, and each backslash by \\.
 *
 */
#include <stdio.h>

int main(void) {
  int c;
  
  printf("Enter control-D to get results\n \n");

  while ( (c = getchar()) != EOF) {
    if (c == '\t') {
      putchar('\\');
      putchar('t');
    } else if (c == '\b') {
      putchar('\\');
      putchar('b');
    } else if (c == '\\') {
      putchar('\\');
      putchar('\\');
    } else {
      putchar(c);
    }
  }

  return 0;
}
