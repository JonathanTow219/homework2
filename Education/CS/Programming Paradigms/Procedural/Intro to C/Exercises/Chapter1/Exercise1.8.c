/*
 * Solution: must only count blanks, tabs, and newlines.
 *
 */
#include <stdio.h>

int main(void) {

  int c, blanks, tabs, nl;

  blanks = 0;
  tabs = 0;
  nl = 0;

  printf("Enter control-D to get results\n\n");

  while ((c = getchar()) != EOF) {
    if (c == '\n') ++nl;
    if (c == '\t') ++tabs;
    if (c == ' ') ++blanks;
  }

  printf("\nNumber of \nnewlines: %d\nblanks: %d\ntabs: %d\n ", nl, blanks, tabs);

  return 0;
}
