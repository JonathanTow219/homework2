
#include <stdio.h>
/**
 * Verify that the expression getChar != EOF is
 * 0 or 1.
 *
 * Solution: simply print out the expression and 
 * check if the value equals 0 (false) or 1 (true).
 * Note that pressing cntrl-D will signal EOF.
 **/
int main(void) {
  
  int c, value;

  printf("Enter control-D to end the program\n\n");
  
  value = ((c = getchar()) != EOF);

  // if value equals 1 than c does not equal EOF 
  // if value equals 0 than c is equal to EOF
  printf("%d", value);

  return 0;
 
}
