#include <stdio.h>

/*  print Fahrenheit-Celsius table for                                                       fahr = 0, 20,.....300; floating-point        
    Exercise 1.4 - Create a table for 
    Celsius to Fahrenheit
*/
main() {
  float celsius, fahr;
  
  int upper, lower, step;
  lower = 0;
  upper = 300;
  step = 20;

  celsius = lower;
  printf("Celsius-Fahrenheit Table\n");

  while(celsius <= upper){
    fahr = (celsius * 1.80) + 32.0;
    printf("%3.0f \t %6.2f\n", celsius,fahr); 
    celsius = step + celsius;
  } 
  
}
