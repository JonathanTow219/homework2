
/* Ex. 1-13
 * Solution: 
 * Write a program to print a histogram of the lengths of words in its input. It is easy to draw the histogram with the bars horizontal; a vertical orientation is more challenging. 
 *
 *
 */

#include <stdio.h>

int main(void) {
  int c, length;
  int length_freq[10];

  printf("Enter control-D to get results\n\n");
  
  length = 0;
  for (int i = 0; i < 10; ++i) 
    length_freq[i] = 0;

  while ((c = getchar()) != EOF) {
    if (c == ' ' || c == '\n' || c == '\t') {
      if(length >= 9) 
	++length_freq[9];
      else
	++length_freq[length];
      length = 0;
    } else {
      ++length;
    }
  }

  printf("\t\t\tFrequency \n");
  printf("\t0     5     10    15    20    25    >= 30\n");
  printf("\t|_____|_____|_____|_____|_____|_____|_____|\n");

  for (int i = 0; i < 10; ++i) {
    if (i == 9) 
      printf(">= 9 \t|");
    else printf("%d \t|", i);

    for (int j = 0; j < length_freq[i] && j <= 35; ++j) {
      printf("-");
    }
    printf("|");
    printf("\n");
  }


  return 0;
}
