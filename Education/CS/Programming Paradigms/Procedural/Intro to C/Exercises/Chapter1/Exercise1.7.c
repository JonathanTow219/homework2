// Ex. 1.7
// Solution: We must only print the value eof here

#include <stdio.h>

int main(void) {
  printf("The value of EOF is %d\n", EOF);

  return 0;
}
