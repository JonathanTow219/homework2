#include <stdio.h>

/* 1.6 program to  count white space, digits, others*/

int main(void) {
  
  int c, i, nwhite, nother;
  int ndigit[10];
  
  nwhite = 0;
  nother = 0;

  printf("Press to control-D to get results");

 for (i = 0; i < 10; i++) {
    ndigit[i] = 0;
  }

  while ((c = getchar()) != EOF) {
    if ( c >= '0' && c <= '9') 
      ++ndigit[c-'0'];
    else if (c == ' ' || c == '\n' || c == '\t') 
      ++nwhite;
    else 
      ++nother;
  }

  printf("digits = ");
  
  for (i = 0; i < 10; i ++) {
    printf("%d ", ndigit[i]);    
  }

}
