/**
 * Here we make the longest-line program
 * using external variables.
 */

#include <stdio.h>
#define MAXLINE 1000

/* external variables */
int max;                            /*maximum length seen so far*/
char line[MAXLINE];         /* current input line */
char longest[MAXLINE];   /* longest line saved here*/

int getLine(void);
void copy(void);

/* program to print the longest line */
int main(void) {

  int len;

  printf("Enter control-D to get result\n");

  while ((len = getLine()) > 0) 
    if (len > max) {
      max = len;
      copy();
    }
  
  if (max > 0)
    printf("Longest Line: %s", longest);

  return 0;
}

int getLine() {

  int c, i;
  /*note that it is redudant to declare these external variables*/
  extern int max;
  extern char longest[];
  
  for (i = 0; i < MAXLINE - 1 && (c = getchar()) != EOF && c != '\n'; ++i)
    line[i] = c;

  if (c == '\n') {
    line[i] = c;
    ++i;
  }
  line[i] = '\0';

  return i;
}

void copy() {

  int i;
  extern char line[], longest[]; /*note that it is redudant to declare these external variables*/
  
  i = 0;
  while ((longest[i] = line[i]) != '\0') 
    ++i;  
}
