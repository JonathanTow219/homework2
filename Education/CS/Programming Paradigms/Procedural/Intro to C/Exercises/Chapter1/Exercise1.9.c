/* Ex. 1.9
 * Solution: Replace each string of one or more 
 * blanks by a single blank
 */

#include <stdio.h>

int main(void) {
  
  int c, inspace;

  printf("Enter control-D to end program\n\n");

  inspace = 0;
 
  while ((c = getchar())  != EOF) {
    if (c == ' ') {
      if ((inspace = getchar()) == ' ')  {
	putchar(' ');
      } else {
	putchar(c);
	putchar(inspace);
      }
    } else {
      putchar(c);  
    }
  }
 
  
  return 0;
}

