#include <stdio.h>

// Exercise 1.2 Experiment to see what happens when you enter \c as an argument
// to printf.
main() {
/* When we had the \c to the printf argument we get a compiler warning that says
   warning: unknown escape sequence '\c'   In C a backslash indicates an escape sequence 
   for characters that are invisible such as new line or in the case of using backslash
*/
  printf("Hello, World! \c");
}
