/*
 * Ex 1-21
 * Solution: Write a program entab that replaces strings of blanks with the minimum number of tabs and blanks to achieve the same spacing. Use the same stops as for detab . When either a tab or a single blank would suffice to reach a tab stop, which should be given preference? 
 *
 *
 * If a tab stop is only a single blank away from a tab stop, 
 * we should give preference to the single blank rather than a
 * tab.
 */

#include <stdio.h>

#define MAXLINE 1000
#define TAB '\t'
#define SPACE ' '

int tabStop;

int getLine(char s[]);
void entab(char s[]);
void minSpacing(int counter, int pos);

int main(void)
{
  int len;
  char line[MAXLINE];
  
  tabStop = 0;
  len = 0;
  
  printf("Enter control-D to exit program\nSet tab stops: ");
  scanf(" %d", &tabStop);

  while ((len = getLine(line)) > 0) {
    entab(line);
  }    
}

/*getLine: read a line into s, return length */
int getLine(char s[])
{
  int i, c;
  
  for (i = 0; i < MAXLINE-1 && (c = getchar()) != EOF && c != '\n'; ++i) 
    s[i] = c;
  if (c == '\n') {
    s[i] = c;
    ++i;
  }
  s[i] = '\0';
  
  return  i;
}

/* entab:  replaces strings of blank spaces w/ minimum amout 
 *             of tabs and spaces to achieve same spacing.
 */
void entab(char s[])
{
  int i, spaceCount, pos;
  
  i = spaceCount = pos = 0;

  while (s[i] != '\0') {
    spaceCount = 0;

    if (s[i] == SPACE && s[i+1] == SPACE) {
      while (s[i] == SPACE) {
	++pos;
	++spaceCount;
	++i;
      }
      minSpacing(spaceCount, pos);
    } else {
      putchar(s[i]);
      ++pos;
      ++i;
    }
  }
}

/*minSpacing: divides the total amount of spaces in a string into
 *             the minimum spaces or tabs needed to get the
 *             same amount of spacing.
 */
void minSpacing(int spaceCount, int pos)
{

  while (spaceCount > 0) {
    if (tabStop - (pos % tabStop) == tabStop || (pos % tabStop))  {
      putchar('t'); // indicates tab use
      pos -= tabStop;    
      spaceCount -= tabStop;
    } else {
    

       putchar('.'); // indicates space use
      --pos;
      --spaceCount;
    }
  }
}

/*
 * Example: t indicates use of tab; . (period) indicates spaces
 * 
 * Set tab stops: 5
 *
 * hey  there  cow     boy
 * hey..there..cowtboy..
 *
 *
 */



