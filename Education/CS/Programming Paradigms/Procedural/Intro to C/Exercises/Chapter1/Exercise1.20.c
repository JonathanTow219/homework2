/*
 * Ex 1-20
 * Solution: Write a program detab that replaces tabs in the input with the proper number of blanks to space to the next tab stop. Assume a fixed set of tab stops, say every n columns. Should n be a variable or a symbolic parameter? 
 *
 * n should be a variable in case a user wants to change the 
 * fixed tabstops in the command-line.
 *
 */

#include <stdio.h>

#define MAXLINE 1000
#define TAB '\t'
#define SPACE ' '

int getLine(char s[]);
int stopDistance(int n, int pos);
void detab(char s[], int n);

int main(void) {

  int n;
  char line[MAXLINE];

  n = 0;
    
  printf("Enter control-D to exit program\nSet tab stops: ");
  scanf(" %d", &n);

  while (getLine(line) > 0) {
    detab(line, n);
  }
    
}

/*getLine: read a line into s, return length */
int getLine(char s[]) {

  int i, c;

  for (i = 0; i < MAXLINE-1 && (c = getchar()) != EOF && c != '\n'; ++i) 
    s[i] = c;

  if (c == '\n') {
    s[i] = c;
    ++i;
  }
  s[i] = '\0';

  return i;
}

/* detab: replaces tabs in the input with n amount of spaces to the next tab stop */
void detab(char s[], int n) {
  int i, j, pos;

  i = pos = 0;
  
  while (s[i] != '\0') {
    if (s[i] == TAB) {
      for (j = 0; j < stopDistance(n, pos); ++j) {
	putchar(SPACE);
      }
      pos += j;
    } else {
      ++pos;
      putchar(s[i]);
    }
    ++i;
  }
}

int stopDistance(int n, int pos) {
  return n - (pos % n);
}

/* Example:
 * Enter control-D to exit program
 * Set tab stops: 3
 *
 *  jon pickles:     dutch       <-- before detab
 *  jon pickles:    dutch        <-- after detab
 *
 *where   is the food?         <-- before detab
 *where     is the food?       <-- after detab
 */
