/* Ex. 1-14
 * Solution: Write a program to print a histogram of the frequencies of different characters in its input. 
 */
#include <stdio.h>

#define MIN_CHAR  33
#define MAX_CHAR  127
#define CHAR_LIMIT MAX_CHAR - MIN_CHAR

int main(void)
{
  int c;
  int differentChar[CHAR_LIMIT];
  
  printf("Enter control-D to get results\n\n");
  
  for (int i = 0; i < CHAR_LIMIT; ++i) {
    differentChar[i] = 0;
  }
  
  while ((c = getchar()) != EOF) {
    if ((c >= MIN_CHAR  && c <= MAX_CHAR)
	&& (c != ' ' || c != '\t' || c != '\n')) {
      ++differentChar[c - MIN_CHAR];
    }
  }

  /* frequency chart's range*/
  printf("\t\t\tFrequency \n");
  printf("\t0     5     10    15    20    25    30    35    40    >= 45\n");
  printf("\t|_____|_____|_____|_____|_____|_____|_____|_____|_____|\n");

  /* frequencies */
  for (int i = 0; i < CHAR_LIMIT; ++i) {
    char ascii = i + MIN_CHAR;
    printf("%c \t|", ascii); //prints ascii character 
    
    for (int j = 0; j < differentChar[i] && j <= 45; ++j) {
      printf("-");
    }
    
    printf("|");
    printf("\n");
  }

  return 0;
}
