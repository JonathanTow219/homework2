#include <stdio.h>

main() {
  printf("Hello, World!\n");

  // alternative way just for learning sake
  printf("Hello, ");
  printf("World! \c");
  printf("\n");
}
