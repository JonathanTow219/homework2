/* Ex. 1-12
 * Solution: print input one word per line.
 */

#include <stdio.h>

int main(void) {
  
  int c;
  
  printf("Enter control-D to get results\n\n");

  while ((c = getchar()) != EOF) {
    if (c == ' ' || c == '\n' || c == '\t') {
      putchar('\n');
    } else {
      putchar(c);
    }
  }

  return 0;
}
