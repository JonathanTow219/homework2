/*
 * Ex 1-15
 * Solution: Rewrite the temperature conversion program of Section 1.2 to use a function for conversion. 
 *
 */
#include <stdio.h>

float fahr_to_cel(float n);

int main(void) {
  int i, upper, lower, inc;
  float fahr;
  
  upper = 300;
  lower = 0;
  inc = 20;
  
  for(i = lower; i <= upper; i += inc) {
    fahr = i;
    printf("%3.0f %6.1f\n", fahr,fahr_to_cel(fahr));
  }
  
  return 0;
}

float fahr_to_cel(float fahr) {
  
  float celsius;

  celsius = (5.0/9.0) * (fahr - 32.0);
  
  return celsius;
}
