/*
  Exercise 1.5 : Modify the temperature conversion program to print the table in reverse
  order that is from, 300 to 0.
*/
#include <stdio.h>

main() {
  
  int fahr;
  
  printf("Fahrenheit to Celsius\n");

  for(fahr = 300; fahr > 0; fahr = fahr - 20) {
    printf("%3d \t %6.2f\n", fahr, (5.0/9.0)*(fahr-32));
  }

}
