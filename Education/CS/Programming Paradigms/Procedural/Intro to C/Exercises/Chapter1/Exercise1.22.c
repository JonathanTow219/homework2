/*
 * Ex. 1-22
 * Solution: Write a program to "fold" long input lines into two or more shorter lines after the last non-blank character that occurs before the n -th column of input. Make sure your program does something intelligent with very long lines, and if there are no blanks or tabs before the specified column. 
 *
 */

#include <stdio.h>

#define MAXLINE 1000
#define COL_WRAP 5

int getLine(char line[], int lim);
void wordWrap(char s[]);

int main(void)
{
  int colWrap;

  char line[MAXLINE];

  colWrap = COL_WRAP;
  
  while (getLine(line, MAXLINE) > 0) {
    wordWrap(line);
    printf("%s\n", line);
  }
}

int getLine(char s[], int lim)
{
  int i, c;

  for (i = 0; i < lim-1 && (c = getchar()) != EOF && c != '\n'; ++i) {
    s[i] = c;
  }

  if (c == '\n') {
    s[i] = c;
    ++i;
  }
  s[i] = '\0';

  return i;
}

void wordWrap(char s[])
{
  int i, j, pos;
  char temp[MAXLINE];

  j = pos = 0;
  
  for (i = 0; s[i] != '\0'; ++i) {
    if (i > 0
	&& i % COL_WRAP == 0
	&& s[i] == ' ') {
      temp[pos] = '\n';
      ++pos;
      ++i;
      temp[pos] = s[i];
    
    } else if ((i % COL_WRAP == 0)
	       && s[i+1] != ' '
	       && s[i] != ' ') {
      while (s[i] != ' ') {
	--pos;
	--i;
      }
      
      temp[pos] = '\n';
      
      ++pos;
      
      while (i % COL_WRAP != 0) {
	temp[pos] = s[i];
	++pos;
	++i;
      }
    } else {    
      temp[pos] = s[i];
    }
    ++pos;
  }

  temp[pos] = '\0';
  
  while (temp[j] != '\0') {
    s[j] = temp[j];
    ++j;
  }
  
  ++j;
  s[j] = '\0';     
    
}

