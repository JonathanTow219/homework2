/*
 * Ex 1-18 
 * Solution: Write a program to remove all trailing blanks and tabs from each line of input, and to delete entirely blank lines. 
 */

#include <stdio.h>
#define MAXLINE 10000

int get_line(char s[], int lim);
void removeTrail(char s[]);

/* print lines without trailing blanks and tabs.*/
int main(void) {
  int len;
  char line[MAXLINE];

  printf("Enter control-D to end program\n");

  while ((len = get_line(line, MAXLINE)) > 0) {
    removeTrail(line);
    printf("%s\n", line);
  }
  return 0;
}

/* get_line: read characters into s, return length */
int get_line(char s[], int lim) {
  int c, i;
  
  for (i = 0; i < lim-1 && (c = getchar()) != EOF && c != '\n'; ++i) 
    if (c == '\t') 
      --i;
    else 
      s[i] = c;
    
  if (c == '\n') {
    s[i] = c;
    ++i;
  }
  s[i] = '\0';

  return i;
}

void removeTrail(char s[]) {
  int i, end;

  for (i = 0; s[i] != '\n'; ++i )
    ;

  --i;

  for (end = i; s[end] == ' '; --end)
    ;
  
  ++end;
  s[end] = '\n';
  ++end;
  s[end] = '\0';
}
