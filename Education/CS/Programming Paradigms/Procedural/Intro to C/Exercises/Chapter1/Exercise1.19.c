/**
 * Ex 1-19
 * Solution: Write a function reverse(s) that reverses the character string s . Use it to write a program that reverses its input a line at a time. 
 */

#include <stdio.h>
#define MAXLINE 1000

int get_line(char s[], int lim);
void reverse(char s[]);

/*print the reverse of a character string*/
int main(void) {
  int len;
  char line[MAXLINE];

  printf("Enter control-D to end program\n");
  
  while ((len = get_line(line, MAXLINE)) != 0) {
     reverse(line);
     printf("%s\n", line);
  }

  return 0;
}

/*get_line : read a line into s, return the size */
int get_line(char s[], int lim) {
  int i, c;
  
  for (i = 0; i < lim-1 && (c = getchar()) != EOF && c != '\n'; ++i) {
    s[i] = c;
  }

  if (c == '\n') {
    s[i] = '\n';
    ++i;
  }
  s[i] = '\0';

  return i;
}

/*reverse : reverse the character string s  */
void reverse(char s[]) {
  int i,  j, sLength;
  
  for (i = 0; s[i] != '\n'; ++i)
    ;
 
  --i; /* decrement i to the element before '\n'*/ 
  char temp[i + 1]; /* temporary array for swapping values */
  sLength = i; /* original length of the string s without '\n' and '\0'*/
  
  for (j = 0;  j <= sLength; ++j) {
    temp[j] = s[i];
    --i;
  }

  while (i < j) {
    s[i] = temp[i];
    ++i;
  }
  
  s[i] = '\n';
  ++i;
  s[i] = '\0';
}
